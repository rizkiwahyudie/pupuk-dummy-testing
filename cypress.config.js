const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  reporter: "mochawesome",
  reporterOptions: {
    reportDir: `report/detail-report`,
    html: false,
    json: true,
    reportFilename: "vocasia_test",
    code: true,
    charts: true
  }
});
